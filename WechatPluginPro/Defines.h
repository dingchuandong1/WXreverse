//
//  Defines.h
//  WechatPluginPro
//
//  Created by dcd on 2017/12/18.
//  Copyright © 2017年 刘伟. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

//QA
//#define API_Base_URL @"amqp://bh_qa:boohee@192.168.1.45:5672"
//#define API_Base_URL_Http @"https://iceberg-land.iboohee.cn/iceland/"

//
#define API_Base_URL @"amqp://lux:oooppp@192.168.6.30:5672"
#define API_Base_URL_Http @"http://192.168.6.30:3000/iceland/"


#define KUserInfo @"userInfo"
#define KIsAccept @"isAccept"
#define KAllFriend @"allFriend"
#define KAppSecret @"jxuvrf4or8xfez2945o4om3vha7azsuz"

#define mUserDefaults       [NSUserDefaults standardUserDefaults]

/*
 请求服务器接口
 1-获取登录的二维码,2-获取群列表,3-获取群成员列表,4-联系人列表,5-接收到新消息,6-用户状态发生改变
 */
typedef NS_ENUM(NSUInteger, CallServerType) {
    CallServerType_GetQRCode,
    CallServerType_GetGroupContactList,
    CallServerType_GetGroupMemberList,
    CallServerType_GetAllFriend,
    CallServerType_BatchAddMsgs,
    CallServerType_StartNotifier
};

/*
 消费服务器接口
 1-登录,2-获取群列表,3-获取群成员列表,4-联系人列表,5-接收到新消息,6-用户状态发生改变
 */
typedef NS_ENUM(NSUInteger, ServerCallType) {
    ServerCallType_Login,
    ServerCallType_GetGroupContactList,
    ServerCallType_GetGroupMemberList,
    ServerCallType_GetAllFriend,
    ServerCallType_BatchAddMsgs,
    ServerCallType_StartNotifier
};
#endif /* Defines_h */
