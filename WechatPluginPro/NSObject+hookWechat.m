//
//  NSObject+hookWechat.m
//  WechatPluginPro
//
//  Created by 刘伟 on 2017/11/29.
//  Copyright © 2017年 刘伟. All rights reserved.
//

#import "NSObject+hookWechat.h"
#import "ServerCallHelper.h"
#import "CallServerHelper.h"

static WCContactData *contactData;
static CallServerHelper *callServerHelper;
static ServerCallHelper *serverCallHelper;

static int loadFinishedCount;//执行skingOnGetOnlineInfoFinished方法的次数（第二次标识好友及群数据已经加载完成）

@implementation NSObject (hookWechat)

+ (void)hookWeChat {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        callServerHelper = [[CallServerHelper alloc] init];
        serverCallHelper = [[ServerCallHelper alloc] init];
        
    });
    
    //微信多开
    sking_hookClassMethod(objc_getClass("CUtility"), @selector(HasWechatInstance), [self class], @selector(skingHasWechatInstance));
    
     //拦截微信发送消息方法  （发送服务器）
    sking_hookMethod(objc_getClass("MessageService"), @selector(SendTextMessage:toUsrName:msgText:atUserList:), [self class], @selector(hook_onSendTextMessage: toUsrName:msgText:atUserList:));
    
    //拦截发送图片
    sking_hookMethod(objc_getClass("MessageService"), @selector(SendImgMessage:toUsrName:thumbImgData:midImgData:imgData:imgInfo:), [self class], @selector(hook_SendImgMessage:toUsrName:thumbImgData:midImgData:imgData:imgInfo:));
    
    //拦截微信撤回消息方法(不用 暂时)
    sking_hookMethod(objc_getClass("MessageService"), @selector(onRevokeMsg:), [self class], @selector(hook_onRevokeMsg:));
    
    //拦截接收到新消息方法  （收到发送服务器）
    sking_hookMethod(objc_getClass("MessageService"), @selector(OnSyncBatchAddMsgs:isFirstSync:), [self class], @selector(hook_OnSyncBatchAddMsgs:isFirstSync:));
    
    //拦截确认好友方法  （主动调用）
     sking_hookMethod(objc_getClass("MMFriendRequestMgr"), @selector(acceptFriendRequestWithFriendRequestData:completion:), [self class], @selector(skingAcceptFriendRequestWithFriendRequestData:completion:));
    
    //拦截添加好友时创建MMFriendRequestData对象方法 （拦截 发送服务器 看返回值 是否调用确认好友）
    sking_hookMethod(objc_getClass("MMFriendRequestData"), @selector(initWithDictionary:), [self class], @selector(skingInitWithDictionary:));
    
    //拦截获取群成员列表  （拦截 发送服务器  ）
     sking_hookMethod(objc_getClass("GroupStorage"), @selector(GetGroupMemberListWithGroupUserName:), [self class], @selector(skingGetGroupMemberListWithGroupUserName:));
    
    //拦截获取群列表方法
     sking_hookMethod(objc_getClass("GroupStorage"), @selector(GetGroupContactList:ContactType:), [self class], @selector(skingGetGroupContactList:ContactType:));
    
    //拦截获取联系人列表
     sking_hookMethod(objc_getClass("ContactStorage"), @selector(GetAllFriendContacts), [self class], @selector(skingGetAllFriendContacts));
    
    //拦截邀请好友入群方法   (主动调用)
    sking_hookMethod(objc_getClass("GroupStorage"), @selector(InviteGroupMemberWithChatRoomName:memberList:completion:), [self class], @selector(skingInviteGroupMemberWithChatRoomName:memberList:completion:));
    
    //拦截拉好友入群方法    （主动调用）
    sking_hookMethod(objc_getClass("GroupStorage"), @selector(AddGroupMembers:withGroupUserName:completion:), [self class], @selector(skingAddGroupMembers:withGroupUserName:completion:));

    //拦截修改备注信息      （主动调用）
    sking_hookMethod(objc_getClass("ContactStorage"), @selector(addOpLog_ModifyContact:sync:), [self class], @selector(skingAddOpLog_ModifyContact:sync:));
    
    //拦截获取登录二维码方法   （发送服务器）
    sking_hookMethod(objc_getClass("QRCodeLoginCGI"), @selector(getQRCodeWithCompletion:), [self class], @selector(skingGetQRCodeWithCompletion:));
    
    //拦截群公告方法        （主动调用）
    sking_hookMethod(objc_getClass("MMChatRoomInfoDetailCGI"), @selector(setChatRoomAnnouncementWithUserName:announceContent:withCompletion:), [self class], @selector(skingSetChatRoomAnnouncementWithUserName:announceContent:withCompletion:));
    
    //尝试拦截获取在线信息方法 （用户掉线，1 通知服务器， 2 关闭进程）
    sking_hookMethod(objc_getClass("Reachability"), @selector(startNotifier), [self class], @selector(skingStartNotifier));
    
    //退出登录(没有拦截到)
    sking_hookMethod(objc_getClass("LogoutCGI"), @selector(sendLogoutCGIWithCompletion:), [self class], @selector(skingSendLogoutCGIWithCompletion:));
    
    //清除登录信息
    sking_hookMethod(objc_getClass("MMLoginOneClickViewController"), @selector(loadView), [self class], @selector(skingLoadView));
    
    //登录时调用一次 登录完成调用一次
    sking_hookMethod(objc_getClass("AccountService"), @selector(onGetOnlineInfoFinished), [self class], @selector(skingOnGetOnlineInfoFinished));
    
}

- (void)skingOnGetOnlineInfoFinished {
    
    //count == 2表示已经获取好各类数据
    loadFinishedCount++;
    if (loadFinishedCount == 2) {
        [callServerHelper setisFinishedLoad:YES];
        [serverCallHelper initQueue];
    }
    [self skingOnGetOnlineInfoFinished];
}


- (void)skingLoadView {
    
//    AccountService *accountService = [[objc_getClass("MMServiceCenter") defaultCenter] getService:objc_getClass("AccountService")];
//    [accountService ClearLastLoginAutoAuthKey];
    [self skingLoadView];
}

/*
 退出登录方法(主动调用 mac)
 */
- (void)skingSendLogoutCGIWithCompletion:(id)arg1 {
    
    [self skingSendLogoutCGIWithCompletion:arg1];
    //通知服务器退出登录了
    [callServerHelper StartNotifier:YES];
}


/*
 清除数据
 */
- (void)hook_ClearData
{
    [self hook_ClearData];
}

/*
 拦截登录状态改变
 */
- (BOOL)skingStartNotifier {
    
    BOOL result = [self skingStartNotifier];
    [callServerHelper StartNotifier:result];
    
    return result;
}


/*
 拦截修改群公告方法
 */
- (void)skingSetChatRoomAnnouncementWithUserName:(id)arg1 announceContent:(id)arg2 withCompletion:(id)arg3 {
    
    [self skingSetChatRoomAnnouncementWithUserName:arg1 announceContent:arg2 withCompletion:arg3];
}

/*
 拦截获取登录二维码方法
 */
- (void)skingGetQRCodeWithCompletion:(QRBlock)arg1 {
    
    [self skingGetQRCodeWithCompletion:^(id argarg1) {
        [callServerHelper GetQRCodeWithCompletion:argarg1];
        arg1(argarg1);
    }];
}


/*
 拦截修改备注方法
 */
- (BOOL)skingAddOpLog_ModifyContact:(id)arg1 sync:(BOOL)arg2 {
    
    BOOL result = [self skingAddOpLog_ModifyContact:arg1 sync:arg2];
//    abort();
    return result;
}


/**
 拦截拉好友入群方法
 */
- (BOOL)skingAddGroupMembers:(NSArray *)arg1 withGroupUserName:(NSString *)arg2 completion:(id(^)(id))arg3 {
    
    BOOL result = [self skingAddGroupMembers:arg1 withGroupUserName:arg2 completion:arg3];
    return result;
}

/**
 拦截发送入群邀请方法
 */
- (BOOL)skingInviteGroupMemberWithChatRoomName:(NSString *)arg1 memberList:(NSArray *)arg2 completion:(id(^)(id))arg2block {
    
    BOOL result = [self skingInviteGroupMemberWithChatRoomName:arg1 memberList:arg2 completion:arg2block];
    return result;
}


/**
 拦截群列表方法
 */
- (NSArray *)skingGetGroupContactList:(unsigned int)arg1 ContactType:(unsigned int)arg2 {
    
    NSArray *result = [self skingGetGroupContactList:arg1 ContactType:arg2];
    if (result.count > 0) {
        [callServerHelper GetGroupContactList:result];
    }
    return result;
}

/**
 拦截获取联系人列表方法
 */
- (NSArray *)skingGetAllFriendContacts {
    
    NSArray *result = [self skingGetAllFriendContacts];
    contactData = result[0];
    
    //全局变量替换 服务器上传增量上传
    if (result.count > 0) {
        [callServerHelper GetAllFriendContacts:result];
    }
    return result;
}


/**
 拦截获取群成员列表方法
 */
- (NSArray *)skingGetGroupMemberListWithGroupUserName:(id)arg1 {
    
    NSArray *result = [self skingGetGroupMemberListWithGroupUserName:arg1];
    if (result.count > 0) {
//        [callServerHelper GetGroupMemberListWithGroupUserName:result andGroupID:arg1];
    }
    return result;
}

/**
拦截添加好友时创建MMFriendRequestData对象方法
 延时5秒自动通过好友验证
 */
- (id)skingInitWithDictionary:(id)arg1 {
    
    id test = [self skingInitWithDictionary:arg1];
    [callServerHelper InitWithDictionary:test];
    return test;
}

/**
 拦截确认好友方法
 */
- (id)skingAcceptFriendRequestWithFriendRequestData:(MMFriendRequestData *)arg1 completion:(id(^)(id))arg2block{
    
    id test = nil;
    test = [self skingAcceptFriendRequestWithFriendRequestData:arg1 completion:arg2block];
    [callServerHelper AcceptFriendRequestWithFriendRequestData:arg1 completion:arg2block];
    return test;
}

/**
 消息同步方法
 */
- (void)hook_OnSyncBatchAddMsgs:(NSArray *)msgs isFirstSync:(BOOL)arg2{
    [callServerHelper BatchAddMsgs:msgs isFirstSync:arg2];
    [self hook_OnSyncBatchAddMsgs:msgs isFirstSync:arg2];
}

/**
 撤回消息自定义方法
*/
- (void)hook_onRevokeMsg:(id)msg {
    [self hook_onRevokeMsg:msg];
    
    NSURL *url = [NSURL URLWithString:@"https://iceberg.iboohee.cn/api/v1/bots"];
    //单例获取全局session
    NSURLSession *session = [NSURLSession sharedSession];
    //写在是加载任务，还有上传和下载两种其他任务
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers) error:nil];
        NSLog(@"%@", dictionary[@"msg"]);
    }];
    [task resume];
    return;
}

/**
 微信多开
 */
+ (BOOL)skingHasWechatInstance {
    return NO;
}

/**
 发送消息
 */
- (id)hook_onSendTextMessage:(id)arg1 toUsrName:(id)arg2 msgText:(id)arg3 atUserList:(id)arg4;
{
    GroupStorage *groupS = [[objc_getClass("MMServiceCenter") defaultCenter] getService:objc_getClass("GroupStorage")];
    NSArray *ary = [groupS GetGroupMemberListWithGroupUserName:@"5622345566@chatroom"];
    NSLog(@"%@",ary);
    //通知服务器
    [callServerHelper SendTextMessage:arg1 toUsrName:arg2 msgText:arg3 atUserList:arg4];
    id res = [self hook_onSendTextMessage:arg1 toUsrName:arg2 msgText:arg3 atUserList:arg4];
    return res;
}

/**
 发送图片消息
 */
- (id)hook_SendImgMessage:(id)arg1 toUsrName:(id)arg2 thumbImgData:(id)arg3 midImgData:(id)arg4 imgData:(id)arg5 imgInfo:(id)arg6;
{
    id res = [self hook_SendImgMessage:arg1 toUsrName:arg2 thumbImgData:arg3 midImgData:arg4 imgData:arg5 imgInfo:arg6];
    return res;
}
@end
